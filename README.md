## Synopsis

This is a Webservice Rest for Avenue Code. The project is based on Spring boot and Spring Data.

I've mapped two entities (Product and Image) and performed CRUD operations.

## Starting Application

To start the project you have to run the following command:

   
```
#!maven
mvn spring-boot:run

```
 
## Using Application

After start the application, to call the endpoints you must import the postman collection below;

https://www.getpostman.com/collections/3b4b8181dcdbd3080e0a

Inside that collection you will be able to perform all select queries and some inserts.

## Tests

To run tests, use the following command:

    mvn test 


## H2 Database

If you want to see the database, follow these instructions:

go to http://localhost:8080/h2-console

In JDBC URL put: jdbc:h2:mem:testdb

User Name put: sa