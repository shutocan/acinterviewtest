package com.avenuecode;

import com.avenuecode.domain.Image;
import com.avenuecode.domain.Product;
import com.avenuecode.domain.repository.ImageRepository;
import com.avenuecode.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minozzi on 13/11/16.
 */
@SpringBootApplication
public class InterviewTestApplication implements CommandLineRunner {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ImageRepository imageRepository;

	public static void main(String[] args) {
		SpringApplication.run(InterviewTestApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		// Insert Product
		Product parentProduct = productRepository.save(new Product("Product 1 Name", "Product 1 Desc"));

		// Insert Product with Parent
		productRepository.save(new Product("Product 2 Name", "Product 2 Desc", parentProduct));
		productRepository.save(new Product("Product 1 Name", "Product 1 Desc", parentProduct));

		// Insert Image
		imageRepository.save(new Image("jpg", parentProduct));
		imageRepository.save(new Image("jpg", parentProduct));
	}
}
