package com.avenuecode.web.controller;

import com.avenuecode.domain.Product;
import com.avenuecode.domain.repository.ProductRepository;
import com.avenuecode.model.ProductNoRelationship;
import com.sun.research.ws.wadl.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minozzi on 13/11/16.
 */
@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Get All Products
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Product>> getProducts(){
        return new ResponseEntity<Iterable<Product>>(productRepository.findAll(), HttpStatus.OK);
    }

    /**
     * Get Product by Id
     * @param id
     * @return Product
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {
        return new ResponseEntity<Product>(productRepository.findOne(id), HttpStatus.OK);
    }

    /**
     * Insert Product
     * @param product
     * @return Product
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Product> insertProduct(@RequestBody Product product) {
        return new ResponseEntity<Product>(productRepository.save(product), HttpStatus.CREATED);
    }

    /**
     * Get All products by Parent Product Id
     * @param id
     * @return
     */
    @RequestMapping(value = "/parent/{id}", method  = RequestMethod.GET)
    public ResponseEntity<List<Product>> getChildProducts(@PathVariable("id") Long id) {
        return new ResponseEntity<List<Product>>(productRepository.findProductsByParentProduct(id), HttpStatus.OK);
    }

    /**
     * Get all products without relationships
     * @return
     */
    @RequestMapping(value = "/norelationship", method = RequestMethod.GET)
    public ResponseEntity<List<ProductNoRelationship>> getAllNoRelationship(){

        List<ProductNoRelationship> productList = new ArrayList<>();

        for (Product product : productRepository.findAll()) {
            productList.add(new ProductNoRelationship(
                    product.getId(), product.getName(), product.getDescription()
            ));
        }

        return new ResponseEntity<List<ProductNoRelationship>>(productList, HttpStatus.OK);
    }

    /**
     * Get Product without relationships by Id
     * @param id
     * @return ProductNoRelationship
     */
    @RequestMapping(value = "/{id}/norelationship", method = RequestMethod.GET)
    public ResponseEntity<ProductNoRelationship> getProductNoRelationship(@PathVariable("id") Long id){
        Product product = productRepository.findOne(id);
        ProductNoRelationship prdNoRelationship =
                new ProductNoRelationship(product.getId(), product.getName(), product.getDescription());
        return new ResponseEntity<ProductNoRelationship>(prdNoRelationship, HttpStatus.OK);
    }

}
