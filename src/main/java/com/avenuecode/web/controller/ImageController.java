package com.avenuecode.web.controller;

import com.avenuecode.domain.Image;
import com.avenuecode.domain.repository.ImageRepository;
import com.avenuecode.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by minozzi on 13/11/16.
 */
@RestController
@RequestMapping("/images")
public class ImageController {

    private final ImageRepository imageRepository;

    @Autowired
    public ImageController(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    /**
     * Get All Images by Product Id
     * @param id
     * @return
     */
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Image>> getImages(@PathVariable("id") Long id) {
        return new ResponseEntity<List<Image>>(imageRepository.findByProduct(id), HttpStatus.OK);
    }

    /**
     * Insert Image
     * @param image
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Image> insertImage(@RequestBody Image image) {
        return new ResponseEntity<Image>(imageRepository.save(image), HttpStatus.CREATED);
    }
}
