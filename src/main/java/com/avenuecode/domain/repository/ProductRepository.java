package com.avenuecode.domain.repository;

import com.avenuecode.domain.Image;
import com.avenuecode.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by minozzi on 13/11/16.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {

    /**
     * Find Products by Parent product Id
     * @param id
     * @return
     */
    @Query("select p from Product p where p.parentProduct.id = :id")
    List<Product> findProductsByParentProduct(@Param("id") Long id);

}
