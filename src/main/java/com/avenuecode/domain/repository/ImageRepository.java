package com.avenuecode.domain.repository;

import com.avenuecode.domain.Image;
import com.avenuecode.domain.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by minozzi on 13/11/16.
 */
public interface ImageRepository extends CrudRepository<Image, Long> {

    /***
     * Find Image by Product
     * @param id
     * @return
     */
    @Query("select i from Image i where i.product.id = :id")
    List<Image> findByProduct(@Param("id") Long id);
}
