package com.avenuecode.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by minozzi on 13/11/16.
 */

@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "product")
    private List<Image> imageList;

    @ManyToOne
    @JoinColumn(name = "parent_product_id")
    private Product parentProduct;

    private String name;

    private String description;

    protected Product() {};

    public Product(String name, String description, Product parentProduct) {
        this.name = name;
        this.description = description;
        this.parentProduct = parentProduct;
    }

    public Product(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(Product parentProduct) {
        this.parentProduct = parentProduct;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }
}
