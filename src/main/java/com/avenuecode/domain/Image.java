package com.avenuecode.domain;

import javax.persistence.*;

/**
 * Created by minozzi on 13/11/16.
 */

@Entity
public class Image {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private String type;

    protected Image(){};

    public Image(String type, Product product) {
        this.type = type;
        this.product = product;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
