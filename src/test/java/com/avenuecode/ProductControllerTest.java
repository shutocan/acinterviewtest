package com.avenuecode;

import com.avenuecode.domain.Product;
import com.avenuecode.domain.repository.ProductRepository;
import com.avenuecode.model.ProductNoRelationship;
import com.avenuecode.web.controller.ProductController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minozzi on 14/11/16.
 * //TODO update the tests using this tutorial as example http://spring.io/guides/tutorials/bookmarks/
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerTest {

	private static final String DEFAULT_PRODUCT_NAME = "Mock Name";
	private static final String DEFAULT_PRODUCT_DESC = "Mock Desc";
	private static final Long DEFAULT_PRODUCT_ID = 1L;

	//@formatter:off
	@Mock private ProductRepository mockRepository;
	//@formatter:on

	private ProductController productController;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);

		productController = new ProductController(mockRepository);
		givenValues();
	}

	@Test
	public void shouldFindAllProducts(){
		thenAssertProductsHasBeenFound(whenFindAll());
	}

	@Test
	public void shouldFindOneProduct(){
		thenAssertProductHasBeenFound(whenFindOne());
	}

	@Test
	public void shouldFindAllProductsByParentId(){
		thenAssertProductsHasBeenFound(whenFindAllByParentProductId());
	}

	@Test
	public void shouldFindAllProductsWithNoRelationship(){
		thenAssertProductsHasBeenFound(whenFindAllWithNoRelationship());
	}

	@Test
	public void shouldFindOneProductWithNoRelationship(){
		thenAssertProductHasBeenFound(whenFindOneNoRelationshp());
	}

	private void thenAssertProductHasBeenFound(Product product) {
		assertProduct(product);
	}

	private void thenAssertProductHasBeenFound(ProductNoRelationship product) {
		assertProduct(product);
	}

	private void thenAssertProductsHasBeenFound(Iterable<Product> products) {
		List<Product> productList = new ArrayList<>();
		products.forEach(productList::add);
		Product product = productList.get(0);

		Assert.assertEquals(productList.size(), 1);

		assertProduct(product);
	}

	private void thenAssertProductsHasBeenFound(List<ProductNoRelationship> products) {

		Assert.assertFalse(products.isEmpty());

		ProductNoRelationship product = products.get(0);

		assertProduct(product);
	}

	private Iterable<Product> whenFindAll() {
		return productController.getProducts().getBody();
	}

	private ProductNoRelationship whenFindOneNoRelationshp() {
		return productController.getProductNoRelationship(DEFAULT_PRODUCT_ID).getBody();
	}

	private List<ProductNoRelationship> whenFindAllWithNoRelationship() {
		return productController.getAllNoRelationship().getBody();
	}

	private Iterable<Product> whenFindAllByParentProductId() {
		return productController.getChildProducts(DEFAULT_PRODUCT_ID).getBody();
	}

	private Product whenFindOne() {
		return mockRepository.findOne(DEFAULT_PRODUCT_ID);
	}

	private void assertProduct(Product product) {
		Assert.assertEquals(product.getId(), DEFAULT_PRODUCT_ID);
		Assert.assertEquals(product.getName(), DEFAULT_PRODUCT_NAME);
		Assert.assertEquals(product.getDescription(), DEFAULT_PRODUCT_DESC);
		Assert.assertNotNull(product.getParentProduct());
		Assert.assertNull(product.getImageList());
	}

	private void assertProduct(ProductNoRelationship product) {
		Assert.assertEquals(product.getId(), DEFAULT_PRODUCT_ID);
		Assert.assertEquals(product.getName(), DEFAULT_PRODUCT_NAME);
		Assert.assertEquals(product.getDescription(), DEFAULT_PRODUCT_DESC);
	}

	private void givenValues() {
		List<Product> productList = new ArrayList<>();
		Product product = new Product(DEFAULT_PRODUCT_NAME, DEFAULT_PRODUCT_DESC);
		product.setId(DEFAULT_PRODUCT_ID);
		product.setParentProduct(product);

		productList.add(product);

		Mockito.when(mockRepository.findAll()).thenReturn(productList);
		Mockito.when(mockRepository.findOne(Mockito.anyLong())).thenReturn(product);
		Mockito.when(mockRepository.findProductsByParentProduct(Mockito.anyLong())).thenReturn(productList);
	}

}
