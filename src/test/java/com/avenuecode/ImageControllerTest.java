package com.avenuecode;

import com.avenuecode.domain.Image;
import com.avenuecode.domain.Product;
import com.avenuecode.domain.repository.ImageRepository;
import com.avenuecode.web.controller.ImageController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minozzi on 14/11/16.
 * //TODO update the tests using this tutorial as example http://spring.io/guides/tutorials/bookmarks/
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageControllerTest {

	private static final Long DEFAULT_PRODUCT_ID = 1L;
	private static final String DEFAULT_IMAGE_TYPE = "jpg";

	//@formatter:off
	@Mock private ImageRepository mockRepository;
	//@formatter:on

	private ImageController imageController;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);

		imageController = new ImageController(mockRepository);
		givenValues();
	}

	@Test
	public void shouldFindAllImagesByProductId(){
		thenAssertImagesHasBeenFound(whenFindAll());
	}

	private void thenAssertImagesHasBeenFound(List<Image> imageList) {
		Image image = imageList.get(0);

		Assert.assertEquals(imageList.size(), 1);
		Assert.assertEquals(image.getType(), DEFAULT_IMAGE_TYPE);
	}

	private List<Image> whenFindAll() {
		return imageController.getImages(DEFAULT_PRODUCT_ID).getBody();
	}

	private void givenValues() {
		List<Image> imageList = new ArrayList<>();
		Image image = new Image("jpg", new Product("Mock Name", "Mock Desc"));

		imageList.add(image);

		Mockito.when(mockRepository.findByProduct(DEFAULT_PRODUCT_ID)).thenReturn(imageList);
	}

}
